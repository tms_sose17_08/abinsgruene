package de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.util.List;

@NamespaceList({
        @Namespace(prefix = "wfs"),
        @Namespace(prefix = "gml"),
        @Namespace(prefix = "fis")
})
@Root(name = "FeatureCollection", strict = false)
public class GruenflaechenResponse {
    @ElementList(name = "member", inline = true, required = false)
    public List<Member> members;

}
