package de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion;

import java.util.List;

public class CoordConversionResponse {
    public List<Geometry> geometries;
}
