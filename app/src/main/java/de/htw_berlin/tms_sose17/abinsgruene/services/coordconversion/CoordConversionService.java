package de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoordConversionService {

    private final String API_BASE_URL = "http://tasks.arcgisonline.com/arcgis/rest/services/Geometry/GeometryServer/";

    private final int TIMOUT = 600;

    private final CoordConversionServiceREST service;

    public CoordConversionService() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(TIMOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(CoordConversionServiceREST.class);
    }

    public CoordConversionResponse fromWGS84(CoordConversionRequest ccreq) {
        try {
            Gson gson = new Gson();
            return service.convert("4326", "25833", gson.toJson(ccreq)).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public CoordConversionResponse toWGS84(CoordConversionRequest ccreq) {
        try {
            Gson gson = new Gson();
            return service.convert("25833", "4326", gson.toJson(ccreq)).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
