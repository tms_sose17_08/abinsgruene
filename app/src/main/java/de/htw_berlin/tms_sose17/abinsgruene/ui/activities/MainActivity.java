package de.htw_berlin.tms_sose17.abinsgruene.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.htw_berlin.tms_sose17.abinsgruene.R;
import de.htw_berlin.tms_sose17.abinsgruene.model.GruenFlaeche;
import de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.GruenflaechenService;

import static de.htw_berlin.tms_sose17.abinsgruene.R.id.map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {


    private final int STROKE_COLOR = Color.argb(255, 0, 192, 0);
    private final int FILL_COLOR = Color.argb(127, 0, 192, 0);
    private final float MARKER_COLOR = BitmapDescriptorFactory.HUE_GREEN;

    private final LatLngBounds BERLIN_BOUNDS = new LatLngBounds(new LatLng(52.366747146625016, 13.093267890113992), new LatLng(52.6528219087849, 13.722833830245024));
    private final LatLng BERLIN = new LatLng(52.52, 13.405);

    private FusedLocationProviderClient mFusedLocationClient;
    private final GruenflaechenService mGfservice = new GruenflaechenService();

    private LatLng mLocation = BERLIN;
    private int mDistance;
    private GoogleMap mMap;

    private final Map<String, GruenFlaeche> mGruenFlaechen = new HashMap<>();

    private final int PERMISSION = 123;
    private final int ZOOM = 15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(getSupportFragmentManager().findFragmentById(R.id.wait));
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void search() {

        mMap.clear();
        LatLng pos = mMap.getCameraPosition().target;
//        // DEBUG: show search area;
//
//        double lat = pos.latitude;
//        double lng = pos.longitude;
//
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int defaultValue = 1;
        mDistance = sharedPref.getInt(getString(R.string.shared_preference_name), defaultValue);
//
//        float[] d = new float[1];
//        Location.distanceBetween(lat, lng, lat + 1, lng, d);
//        double dlat = (mDistance / d[0]);
//        Location.distanceBetween(lat, lng, lat, lng + 1, d);
//        double dlng = (mDistance / d[0]);
//
//        mMap.addPolygon(new PolygonOptions()
//                .add(new LatLng(pos.latitude -dlat, pos.longitude - dlng),
//                        new LatLng(pos.latitude - dlat, pos.longitude + dlng),
//                        new LatLng(pos.latitude + dlat, pos.longitude + dlng),
//                        new LatLng(pos.latitude + dlat, pos.longitude - dlng),
//                        new LatLng(pos.latitude - dlat, pos.longitude - dlng))
//                .strokeColor(Color.RED));


        new FindGruenflaechenTask().execute(pos);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setLatLngBoundsForCameraTarget(BERLIN_BOUNDS);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, ZOOM));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            mLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, ZOOM));
                        }
                    }
                });
    }

    private class FindGruenflaechenTask extends AsyncTask<LatLng, Void, List<GruenFlaeche>> {
        @Override
        protected void onPreExecute() {
            //mMap.clear();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.hide(getSupportFragmentManager().findFragmentById(R.id.map));
            ft.commit();

            ft = getSupportFragmentManager().beginTransaction();
            ft.show(getSupportFragmentManager().findFragmentById(R.id.wait));
            ft.commit();

        }

        @Override
        protected List<GruenFlaeche> doInBackground(LatLng... pos) {
            return mGfservice.getInArea(pos[0], mDistance);
        }

        @Override
        protected void onPostExecute(List<GruenFlaeche> gfs) {

            for (GruenFlaeche gf : gfs) {
                for (List<Double> coords : gf.coords) {
                    PolygonOptions po = new PolygonOptions();
                    for (int i = 0; i < coords.size(); i += 2) {
                        double x = coords.get(i);
                        double y = coords.get(i + 1);
                        po.add(new LatLng(y, x));
                    }
                    po.strokeColor(STROKE_COLOR);
                    po.fillColor(FILL_COLOR);
                    mMap.addPolygon(po);
                }
                mMap.addMarker(new MarkerOptions()
                        .position(gf.centroid())
                        .title(gf.name)
                        .snippet(gf.nameZusatz)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                mGruenFlaechen.put(gf.name, gf);
            }

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.hide(getSupportFragmentManager().findFragmentById(R.id.wait));
            ft.commit();

            ft = getSupportFragmentManager().beginTransaction();
            ft.show(getSupportFragmentManager().findFragmentById(map));
            ft.commit();
        }
    }

}
