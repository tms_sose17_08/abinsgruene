package de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CoordConversionServiceREST {
    @FormUrlEncoded
    @POST("project?transformation=&transformForward=true&f=pjson")
    Call<CoordConversionResponse> convert(@Field("inSR") String inSR, @Field("outSR") String outSR, @Field("geometries") String geometries);

}
