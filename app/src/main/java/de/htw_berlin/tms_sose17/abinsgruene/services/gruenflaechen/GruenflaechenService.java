package de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen;

import android.annotation.SuppressLint;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.htw_berlin.tms_sose17.abinsgruene.model.GruenFlaeche;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionRequest;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionResponse;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionService;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.Geometry;
import de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse.GruenflaechenResponse;
import de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse.Member;
import de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse.SurfaceMember;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class GruenflaechenService {

    private final String API_BASE_URL = "http://fbinter.stadt-berlin.de/fb/wfs/geometry/senstadt/";

    private final GruenflaechenServiceWFS service;

    private final int TIMOUT = 600;

    private final CoordConversionService ccs = new CoordConversionService();

    public GruenflaechenService() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(TIMOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        service = retrofit.create(GruenflaechenServiceWFS.class);
    }

    public List<GruenFlaeche> getInArea(LatLng pos, int meters) {
        List<GruenFlaeche> result = new ArrayList<>();

        try {

            double lat = pos.latitude;
            double lng = pos.longitude;

            float[] d = new float[1];
            Location.distanceBetween(lat, lng, lat + 1, lng, d);
            double dlat = (meters / d[0]);
            Location.distanceBetween(lat, lng, lat, lng + 1, d);
            double dlng = (meters / d[0]);

            double minlat = pos.latitude - dlat;
            double maxlat = pos.latitude + dlat;
            double minlng = pos.longitude - dlng;
            double maxlng = pos.longitude + dlng;

            CoordConversionRequest ccreq = new CoordConversionRequest();
            ccreq.geometries.add(new Geometry(minlng, minlat));
            ccreq.geometries.add(new Geometry(maxlng, maxlat));
            CoordConversionResponse ccres = ccs.fromWGS84(ccreq);

            minlat = ccres.geometries.get(0).y;
            minlng = ccres.geometries.get(0).x;
            maxlat = ccres.geometries.get(1).y;
            maxlng = ccres.geometries.get(1).x;

            @SuppressLint("DefaultLocale")
            GruenflaechenResponse gfr = service.getGruenanlagenBB(String.format("%f,%f,%f,%f", minlng, minlat, maxlng, maxlat)).execute().body();

            if (gfr != null && gfr.members != null)
                for (Member m : gfr.members) {

                    GruenFlaeche gf = new GruenFlaeche();
                    for (SurfaceMember sm : m.surfaceMember) {
                        String poslist = sm.posList;
                        String[] coords = poslist.split("\\s");

                        ccreq = new CoordConversionRequest();

                        for (int i = 0; i < coords.length; i += 2) {
                            double x = Double.parseDouble(coords[i]);
                            double y = Double.parseDouble(coords[i + 1]);
                            ccreq.geometries.add(new Geometry(x, y));
                        }

                        ccres = ccs.toWGS84(ccreq);

                        List<Double> coordList = new ArrayList<>();

                        //if (ccres != null && ccres.geometries != null)
                        for (Geometry g: ccres.geometries) {
                            if (g == null) continue;
                            double x = g.x;
                            double y = g.y;
                            coordList.add(x);
                            coordList.add(y);
                        }
                        gf.coords.add(coordList);
                    }

                gf.name = m.namenr;
                gf.nameZusatz = m.namezusatz;

                result.add(gf);
            }

            return result;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
