package de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion;

public class Geometry {
    public double x;
    public double y;

    public Geometry() {
    }

    public Geometry(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
