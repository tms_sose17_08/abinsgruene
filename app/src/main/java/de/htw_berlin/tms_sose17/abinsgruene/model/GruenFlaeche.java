package de.htw_berlin.tms_sose17.abinsgruene.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class GruenFlaeche {
    public String name;
    public String nameZusatz;

    // Oh CheckStyles, why do you do this?
    private final int MAGIC_SIX = 6;

    public final List<List<Double>> coords = new ArrayList<>();

    public LatLng centroid() {
        double[] cx = new double[coords.size()];
        double[] cy = new double[coords.size()];
        double[] signedArea = new double[coords.size()];
        double signedAreaSum = 0;
        double rcx = 0, rcy = 0;
        for (int i = 0; i < coords.size(); i++) {
            cx[i] = 0;
            cy[i] = 0;
            signedArea[i] = 0;
            for (int j = 0; j < coords.get(i).size(); j += 2) {
                double x0 = coords.get(i).get(j);
                double y0 = coords.get(i).get(j + 1);
                double x1 = coords.get(i).get((j + 2)     % coords.get(i).size());
                double y1 = coords.get(i).get((j + 2 + 1) % coords.get(i).size());
                double a = x0 * y1 - x1 * y0;
                signedArea[i] += a;
                cx[i] += (x0 + x1) * a;
                cy[i] += (y0 + y1) * a;
            }
            signedArea[i] /= 2;
            cx[i] /= MAGIC_SIX * signedArea[i];
            cy[i] /= MAGIC_SIX * signedArea[i];
            signedAreaSum += Math.abs(signedArea[i]);
        }
        for (int i = 0; i < coords.size(); i++) {
            double weight = Math.abs(signedArea[i]) / signedAreaSum;
            rcx += cx[i] * weight;
            rcy += cy[i] * weight;
        }
        return new LatLng(rcy, rcx);
    }

}
