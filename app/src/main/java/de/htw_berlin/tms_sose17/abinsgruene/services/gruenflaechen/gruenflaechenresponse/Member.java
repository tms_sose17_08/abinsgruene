package de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@NamespaceList({
        @Namespace(prefix = "wfs"),
        @Namespace(prefix = "gml"),
        @Namespace(prefix = "fis")
})
@Root(strict = false)
public class Member {

    @Path("re_gruenanlagenbestand/spatial_geometry/MultiSurface")
    @ElementList(entry = "surfaceMember", inline = true, required = false)
    public List<SurfaceMember> surfaceMember;

    @Path("re_gruenanlagenbestand")
    @Element(name = "NameNr", required = false)
    public String namenr;

    @Path("re_gruenanlagenbestand")
    @Element(name = "NameZusatz", required = false)
    public String namezusatz;

}
