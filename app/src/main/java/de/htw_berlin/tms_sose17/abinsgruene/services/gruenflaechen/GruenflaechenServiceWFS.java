package de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen;

import de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse.GruenflaechenResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GruenflaechenServiceWFS {
    @GET("re_gruenanlagenbestand?service=wfs&version=2.0.0&request=getFeature&Typenames=fis:re_gruenanlagenbestand")
    Call<GruenflaechenResponse> getGruenanlagenBB(@Query("bbox")String bbox);
}
