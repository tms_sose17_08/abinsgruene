package de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion;

import java.util.ArrayList;
import java.util.List;

public class CoordConversionRequest {
    private final String geometryType = "esriGeometryPoint";
    public final  List<Geometry> geometries = new ArrayList<>();
}
