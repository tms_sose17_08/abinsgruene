package de.htw_berlin.tms_sose17.abinsgruene.services.gruenflaechen.gruenflaechenresponse;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@NamespaceList({
        @Namespace(prefix = "wfs"),
        @Namespace(prefix = "gml"),
        @Namespace(prefix = "fis")
})
@Root(strict = false)
public class SurfaceMember {

    @Path("Polygon/exterior/LinearRing")
    @Element(name = "posList")
    public String posList;

    @Path("Polygon/interior/LinearRing")
    @Element(name = "posList")
    public String innerPosList;
}
