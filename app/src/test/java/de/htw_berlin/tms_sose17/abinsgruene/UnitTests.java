package de.htw_berlin.tms_sose17.abinsgruene;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Test;

import java.util.Arrays;

import de.htw_berlin.tms_sose17.abinsgruene.model.GruenFlaeche;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionRequest;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionResponse;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.CoordConversionService;
import de.htw_berlin.tms_sose17.abinsgruene.services.coordconversion.Geometry;

import static org.junit.Assert.assertEquals;

public class UnitTests {

    private static final double DELTA = 1e-10;

    final double BERLIN_LAT_WGS84 = 52.52;
    final double BERLIN_LNG_WGS84 = 13.405;

    final double BERLIN_LAT_ETRS89 = 5820072.159188349;
    final double BERLIN_LNG_ETRS89 = 391779.25925386546;

    @Test
    public void fromWGS84IsCorrect() throws Exception {
        CoordConversionService ccs = new CoordConversionService();

        CoordConversionRequest ccreq = new CoordConversionRequest();
        ccreq.geometries.add(new Geometry(BERLIN_LNG_WGS84, BERLIN_LAT_WGS84));
        CoordConversionResponse ccres = ccs.fromWGS84(ccreq);

        double resultLat = ccres.geometries.get(0).y;
        double resultLng = ccres.geometries.get(0).x;

        assertEquals(BERLIN_LAT_ETRS89, resultLat, DELTA);
        assertEquals(BERLIN_LNG_ETRS89, resultLng, DELTA);

    }

    @Test
    public void toWGS84isCorrect() throws Exception {
        CoordConversionService ccs = new CoordConversionService();

        CoordConversionRequest ccreq = new CoordConversionRequest();
        ccreq.geometries.add(new Geometry(BERLIN_LNG_ETRS89, BERLIN_LAT_ETRS89));
        CoordConversionResponse ccres = ccs.toWGS84(ccreq);

        double resultLat = ccres.geometries.get(0).y;
        double resultLng = ccres.geometries.get(0).x;

        assertEquals(BERLIN_LAT_WGS84, resultLat, DELTA);
        assertEquals(BERLIN_LNG_WGS84, resultLng, DELTA);
    }

    @Test
    public void controidIsCorrect() throws Exception {
        GruenFlaeche gf = new GruenFlaeche();
        gf.coords.add(Arrays.asList(new Double[]{
                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0
        }));
        gf.coords.add(Arrays.asList(new Double[]{
                1.0, 0.0,
                3.0, 0.0,
                3.0, 1.0,
                1.0, 1.0
        }));
        LatLng centroid = gf.centroid();

        final double expectedLat = 0.5;
        final double expectedLng = 1.5;

        assertEquals(expectedLat, centroid.latitude, DELTA);
        assertEquals(expectedLng, centroid.longitude, DELTA);
    }

}