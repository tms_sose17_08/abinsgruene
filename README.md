# **Ab ins Grüne** #
* * *
In Berlin passiert es schnell, dass einem der graue Großstadtalltag zu viel wird.
Abhilfe schafft da leicht ein Spaziergang durch den nächstgelegen Park. Doch nicht immer kennt man sich in seiner aktuellen Umgebung aus und weiß nicht wo dieser zu finden ist.Genau dann hilft Ab Ins Grüne! Hiermit ist es ein leichtes sämtliche Grünanlagen an der Umgebung, oder auch den schnellsten Weg vom aktuellen Standort zu seinem Lieblingspark zu finden.
